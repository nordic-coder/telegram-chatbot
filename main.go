package main

import (
	"os"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Commands = []cli.Command{
		{
			Name: "a",
			Action: func(c *cli.Context) error {
				return botA()
			},
		},
		{
			Name: "b",
			Action: func(c *cli.Context) error {
				return botB()
			},
		},
	}

	app.Run(os.Args)
}
