module gitlab.com/nordic-coder/telegram-chatbot

go 1.12

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/kr/pretty v0.1.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/urfave/cli v1.20.0
)
