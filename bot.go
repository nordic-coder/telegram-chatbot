package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

var key = "828442506:AAEyUKX5Iwh9irQKvufYoIlNkBhJ2e8AQGM"

type resultUpdates struct {
	Ok     bool `json:"ok"`
	Result []struct {
		UpdateID int `json:"update_id"`
		Message  struct {
			MessageID int `json:"message_id"`
			From      struct {
				ID           int    `json:"id"`
				IsBot        bool   `json:"is_bot"`
				FirstName    string `json:"first_name"`
				LastName     string `json:"last_name"`
				Username     string `json:"username"`
				LanguageCode string `json:"language_code"`
			} `json:"from"`
			Chat struct {
				ID        int    `json:"id"`
				FirstName string `json:"first_name"`
				LastName  string `json:"last_name"`
				Username  string `json:"username"`
				Type      string `json:"type"`
			} `json:"chat"`
			Date int    `json:"date"`
			Text string `json:"text"`
		} `json:"message"`
	} `json:"result"`
}

func getURLUpdates(offset int) string {
	return fmt.Sprintf("https://api.telegram.org/bot%v/getupdates?timeout=300&offset=%v", key, offset)
}

func getURLSendMessage(chatID int, text string) string {
	return fmt.Sprintf("https://api.telegram.org/bot%v/sendMessage?chat_id=%v&text=%v", key, chatID, text)
}

func getURLSendPhoto(chatID int, url string) string {
	return fmt.Sprintf("https://api.telegram.org/bot%v/sendPhoto?chat_id=%v&photo=%v", key, chatID, url)
}

func requestTelegram(url string) []byte {

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		panic(err)
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err)
	}

	return body
}

func botA() error {

	urlUpdates := getURLUpdates(0)

	for {
		body := requestTelegram(urlUpdates)

		result := new(resultUpdates)

		json.Unmarshal(body, &result)

		for _, message := range result.Result {
			log.Println(message.Message.Text)

			imageURL, err := findImage(message.Message.Text)

			if err != nil {
				urlSendMessage := getURLSendMessage(message.Message.Chat.ID, err.Error())
				requestTelegram(urlSendMessage)
			} else {
				urlSendPhoto := getURLSendPhoto(message.Message.Chat.ID, imageURL)
				requestTelegram(urlSendPhoto)
			}

			urlUpdates = getURLUpdates(message.UpdateID + 1)
		}
	}

}

func botB() error {
	log.Println("bot b")
	return nil
}

func findImage(keyword string) (resultURL string, err error) {
	url := fmt.Sprintf("https://pixabay.com/api?key=11970146-5463c1c2cb7a671e225d7575e&q=%v", keyword)

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return
	}

	type Result struct {
		TotalHits int `json:"totalHits"`
		Hits      []struct {
			LargeImageURL   string `json:"largeImageURL"`
			WebformatHeight int    `json:"webformatHeight"`
			WebformatWidth  int    `json:"webformatWidth"`
			Likes           int    `json:"likes"`
			ImageWidth      int    `json:"imageWidth"`
			ID              int    `json:"id"`
			UserID          int    `json:"user_id"`
			Views           int    `json:"views"`
			Comments        int    `json:"comments"`
			PageURL         string `json:"pageURL"`
			ImageHeight     int    `json:"imageHeight"`
			WebformatURL    string `json:"webformatURL"`
			Type            string `json:"type"`
			PreviewHeight   int    `json:"previewHeight"`
			Tags            string `json:"tags"`
			Downloads       int    `json:"downloads"`
			User            string `json:"user"`
			Favorites       int    `json:"favorites"`
			ImageSize       int    `json:"imageSize"`
			PreviewWidth    int    `json:"previewWidth"`
			UserImageURL    string `json:"userImageURL"`
			PreviewURL      string `json:"previewURL"`
		} `json:"hits"`
		Total int `json:"total"`
	}

	result := new(Result)

	err = json.Unmarshal(body, &result)
	if err != nil {
		return
	}

	if len(result.Hits) > 0 {
		resultURL = result.Hits[0].LargeImageURL
	} else {
		err = errors.New("Không tìm thấy hình ảnh nào")
	}

	return
}
